#include "mainwindow.h"
#include "ui_mainwindow.h"
#define port 4545
int klient_cislo = 1;
int pocet_klientu;
QStringList datagram_list;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(port, QUdpSocket::ShareAddress);
    connect(udpSocket, SIGNAL(readyRead()),this, SLOT(processPendingDatagrams()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_start_clicked()
{
    pocet_klientu = ui->pocet_klientu->text().toInt();
}

void MainWindow::processPendingDatagrams()
{
    //qWarning() << "tady";
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(datagram.data(), datagram.size());
        qWarning() << datagram;
        QString datagram_qstr(datagram);
        datagram_list = datagram_qstr.split(" ");

    }
    if(datagram_list[0] == "s"){
        int cislo_k_vysilani = datagram_list[1].toInt() + 1;
        if(cislo_k_vysilani > pocet_klientu){
            cislo_k_vysilani = 1;
        }
        qWarning() << cislo_k_vysilani;
        QString datagram_qstr =  "k " + QString::number(cislo_k_vysilani) + " " + datagram_list[2];
        QByteArray datagram = datagram_qstr.toUtf8();
        udpSocket->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, port);
    }

}
