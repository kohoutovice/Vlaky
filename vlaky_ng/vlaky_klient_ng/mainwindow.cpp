#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsView>
#include <QTimer>
#include <QDebug>
#include <QMouseEvent>
#include <QMessageBox>
#include <QMainWindow>
#include <QVector>
#include <QLabel>
#include <QtGui>
#include <QtCore>
#include <QGridLayout>
#include <QtWidgets>

#define SERVER "http://vlaky-siriuscloud.rhcloud.com"
#define vyska 220
#define sirka 220

QList< QLabel* > vlaky;
QList< int > request_were_sent;

int cislo_klienta = 0;
int i = 0;

QTimer *timer = new QTimer;
QTimer *timer_get = new QTimer;
QStringList datagram_list;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(posun()));
    connect(timer_get,SIGNAL(timeout()),this,SLOT(getFile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getFile(){
    if(!http_proccesing)
        downloadFile(QString(SERVER) + "/trains.txt");
}

void MainWindow::posun(){
    for(int i = 0;i < vlaky.size(); i++){
        vlaky[i]->setGeometry(vlaky[i]->x() + 1, vlaky[i]->y(),sirka,vyska);

        if((vlaky[i]->x() + vlaky[i]->width()) > window()->width() && request_were_sent[i] == 0){
            QString send_url = QString(SERVER) + "/add_train.php/?id=" +
                    QString::number(cislo_klienta + 1) +
                    "&y=" +
                    QString::number(vlaky[i]->y());
            qWarning() << "remove_train query" << send_url;
            send_get_request(send_url);

            request_were_sent[i] = 1;
        }
        if( vlaky[i]->x()  > window()->width()){
            remove_train(i);
        }
    }
}

void MainWindow::remove_train(int idx){
    vlaky.removeAt(idx);
    request_were_sent.removeAt(idx);
}

void MainWindow::add_train(int x,int y){
    QPixmap vlak_image("vlacek.png");
    vlaky << new QLabel( this );
    request_were_sent << 0;
    vlaky.last()->setPixmap(vlak_image);
    vlaky.last()->setGeometry(x,y,sirka,vyska);
    layout()->addWidget(vlaky.last());
    send_get_request( QString(SERVER) + "/remove_train.php/?id=" + QString::number(cislo_klienta));
}

void MainWindow::on_pushButton_clicked()
{
    cislo_klienta = ui->cislo_klienta->text().toInt();
    ui->cislo_klienta->setVisible(false);
    ui->pushButton->setVisible(false); 
    ui->pushButton_2->setVisible(false);
    timer->start(20);
    timer_get->start(500);
    send_get_request(QString(SERVER) + "/train_count.php?count=" + QString::number(cislo_klienta));
}

void MainWindow::mouseReleaseEvent ( QMouseEvent * event )
{
    QPixmap vlak_image("vlacek.png");
    vlak_image.scaledToWidth(sirka);
    vlak_image.scaledToHeight(vyska);

    if(cislo_klienta != 0){
        if(event->button() == Qt::RightButton)
        {
            qWarning() << event->x();
            qWarning() << event->y();
            add_train(event->x(),event->y());
        }
    }
}

void MainWindow::send_get_request(QString qurl){

    qnetm_list << new QNetworkAccessManager;
    url_get = QUrl::fromUserInput(qurl);
    qWarning() << url_get;
    ui->statusBar->showMessage(qurl,1000);

    qnetm_list.last()->get(QNetworkRequest(QUrl(qurl)));
}

void MainWindow::httpFinished_get(){
    qWarning() << "http_get finnished";
}

void MainWindow::startRequest(const QUrl &requestedUrl)
{
    url = requestedUrl;
    httpRequestAborted = false;

    reply = qnam.get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, this, &MainWindow::httpFinished);
    connect(reply, &QIODevice::readyRead, this, &MainWindow::httpReadyRead);
}

void MainWindow::downloadFile(QString url)
{
    http_proccesing = true;
    url = url.trimmed();
    const QUrl newUrl = QUrl::fromUserInput(url);
    if (!newUrl.isValid()) {
        qWarning() << tr("Invalid URL: %1: %2").arg(url, newUrl.errorString());
        return;
    }
    // schedule the request
    startRequest(newUrl);
}

void MainWindow::parseString(QString qstr){
    QStringList qstrl = qstr.split("\n");
    for(int i = 0; i < qstrl.size();i++){
        QStringList line = QString(qstrl[i]).split(";");
        if(cislo_klienta == QString(line[0]).toInt()){
            add_train(0 - sirka,QString(line[1]).toInt());
        }
    }
}

void MainWindow::httpFinished()
{
    parseString(file_content);

    file_content.clear();
    reply->deleteLater();
    reply = Q_NULLPTR;
    http_proccesing = false;
}

void MainWindow::httpReadyRead()
{
    file_content += reply->readAll();
}

void MainWindow::on_pushButton_2_clicked()
{
    send_get_request(QString(SERVER) + "/clear.php");
}
